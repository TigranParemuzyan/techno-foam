<?php

namespace AppBundle\Form;

use AppBundle\Entity\Settings;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{

    private $container;
    private $i;
    private $toClassType;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->i =0;
        $this->toClassType = [
            'AppBundle\Entity\Pages'=>[Settings::IS_DOCUMENT=>'Document', Settings::IS_DOCUMENTS_LIST=>'Documents list', Settings::IS_TEXT=>'Text', Settings::IS_TEXT_AREA=>'Long text',
                Settings::IS_BOOLEAN=>'On/Off', Settings::IS_FILE=>'File', Settings::IS_IMAGE=>'Image', Settings::IS_VIDEO=>'Video', Settings::IS_DATE=>'Date', Settings::IS_GALLERY=>'Gallery'],
            'AppBundle\Entity\Document'=>[Settings::IS_TEXT=>'Text', Settings::IS_TEXT_AREA=>'Long text',
                Settings::IS_BOOLEAN=>'On/Off', Settings::IS_FILE=>'File', Settings::IS_IMAGE=>'Image', Settings::IS_VIDEO=>'Video', Settings::IS_GALLERY=>'Gallery'],
            'AppBundle\Entity\Products'=>[Settings::IS_TEXT=>'Text', Settings::IS_TEXT_AREA=>'Long text',
                Settings::IS_BOOLEAN=>'On/Off', Settings::IS_FILE=>'File', Settings::IS_IMAGE=>'Image', Settings::IS_VIDEO=>'Video'],
            'AppBundle\Entity\Projects'=>[Settings::IS_DOCUMENTS_LIST=>'Documents list', Settings::IS_TEXT=>'Text', Settings::IS_TEXT_AREA=>'Long text',
                Settings::IS_BOOLEAN=>'On/Off', Settings::IS_FILE=>'File', Settings::IS_IMAGE=>'Image', Settings::IS_VIDEO=>'Video', Settings::IS_GALLERY=>'Gallery', Settings::IS_URL=>'Link' ],
            'AppBundle\Entity\DocumentsList'=>[Settings::IS_DOCUMENT=>'Document',Settings::IS_TEXT=>'Text', Settings::IS_TEXT_AREA=>'Long text',
                Settings::IS_BOOLEAN=>'On/Off', Settings::IS_FILE=>'File', Settings::IS_IMAGE=>'Image', Settings::IS_VIDEO=>'Video', Settings::IS_DATE=>'Date']


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name')
            ->add('toClassType', 'choice', ['choices'=>
                $this->toClassType[$options['from_class_name']]
            ])
            ->add('position', 'hidden', ['data'=>$this->i, 'attr'=>['sortable'=>1]])
            ->add('inEnabled')
            ->add('fromId', 'hidden', ['data'=>(int)$options['from_id']])
            ->add('toId', 'hidden')
            ->add('fromClassName', 'hidden', ['data'=>$options['from_class_name']])
            ->add('toClassName', 'hidden')

            ->addModelTransformer(new CallbackTransformer(

                function ($tagsAsArray) {
                    // transform the array to a string
                    return $tagsAsArray;
                },
                function ($beforeParsist) {

                    $this->i ++;
                    if((int)$beforeParsist->getPosition() == 0){
                        $beforeParsist->setPosition($this->i);
                    }

                    if($beforeParsist->getFromClassName() && $beforeParsist->getToClassType() >=0 && $beforeParsist->getFromId()){
                        $beforeParsist = $this->container->get('app.document.settings')->createDocument($beforeParsist, $this->i);
                    }

                    // transform the string back to an array
                    return $beforeParsist;
                }
            ))
        ;

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Settings',
            'from_id'=>null,
            'from_class_name'=>null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_settings';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_settings_type';
    }
}
