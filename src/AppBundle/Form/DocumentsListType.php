<?php

namespace AppBundle\Form;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentsListType extends AbstractType
{
    private $container;
    private $em;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['data'];

        $settings = $this->em->getRepository('AppBundle:Settings')->findFrom($data->getId(), $options['data_class']);

        if(!is_null($settings)){
            foreach ($settings as $setting){
                $data->addFromSettings($setting);
            }
            $builder
                ->add('fromSettings', 'app_bundle_parameters', ['from_id'=>$data->getId(), 'from_class_name'=> $options['data_class'],
                    'data'=>$settings, 'mapped' => false,]);
        }



        $builder
            ->add('title', 'hidden')
            ->add('slug', 'hidden')
            ->add('sortOrdering', 'hidden')
            ->add('created')
            ->add('updated')
            ->add('formType', 'hidden');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\DocumentsList',
            'form_type_text'=>null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bundle_documents_list';
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_bundle_documents_list';
    }


}
