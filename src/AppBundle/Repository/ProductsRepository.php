<?php

namespace AppBundle\Repository;

/**
 * ProductsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductsRepository extends \Doctrine\ORM\EntityRepository
{

    public function findQuick() {

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:Products', 'p')
            ->innerJoin('p.service', 'ps')->addSelect('ps')
            ->leftJoin('ps.service', 's')->addSelect('s')
            ->orderBy('p.id')
            ->getQuery()->getResult()
            ;
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $orderBy); // TODO: Change the autogenerated stub


    }
}
